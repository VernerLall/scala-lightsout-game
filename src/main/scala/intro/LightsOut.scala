package intro;

import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.all._
import org.scalajs.dom._

object LightsOut{
  case class State(board: Seq[Boolean], clicks: Int, text: String, genClicks: Int)
  def newGame(clicks: Int) = {
    var state = State(Seq.fill(5*5)(false), 0, "", 0)
    val r = new scala.util.Random
    for (i <- 0 until clicks){
      state = click(state, r.nextInt(24))
    }
    State(state.board, 0, state.clicks.toString, state.clicks)
  }

  def click(s: State, index: Int) = {
    def toxy(index: Int) = (index % 5, index / 5)
    def inboard(index: Int) = {
      def f(i: Int) = {
        val (x, y) = toxy(index)
        val (px, py) = toxy(i)
        !(px != x && py != y || (px<0 || px>4 || py<0 || py>4))
      }
      List[Int](-5,-1,0,1,5).map( _+index ).filter(f)
    }
    val pattern = inboard(index)
    val board = s.board.zipWithIndex.map{ case (e,i) => {
      if (pattern.contains(i))
        !e
      else
        e
    }}
    State(
      board,
      s.clicks + 1,
      s.text,
      s.genClicks
    )
  }

  class Backend($: BackendScope[Unit, State]) {

    def handleNewGame(clicks: Int) = $.modState( s => newGame(clicks))


    def handleClick(index: Int) = $.modState( s => {
      click(s, index)
    })

    def renderRuut(x: Boolean, index: Int) = {
      val cn = if (x) {"ruut active"} else {"ruut"}
      div(
        key := index,
        onClick --> handleClick(index),
        className := cn,
        id := s"ruut_${index}"
      )
    }

    def renderRida(rida: Seq[Boolean], y: Int) = {
      val ruudud = rida.zipWithIndex.map{case(item, i) =>
        renderRuut(item, i + (y * 5))
      }
      div(
        className := "rida",
        key := y,
        ruudud.toVdomArray
      )
    }

    def renderBoard(board: Seq[Boolean]) = {
      val board_read = board.sliding(5, 5).toList
      val read = board_read.zipWithIndex.map{ case(e, y) => {
        renderRida(e, y)
      }}
      div(
        className := "board",
        read.toVdomArray
      )
    }

    def onChangeHandler(e: ReactEventFromInput) = {
      def isAllDigits(x: String) = x forall Character.isDigit
      val newValue = e.target.value
      if (isAllDigits(newValue))
        if (newValue.toInt < 1001)
          $.modState(_.copy(text = newValue))
        else
          $.modState(_.copy(text = "1000"))

      else
        $.modState(_.copy(text = "1"))
    }

    def handleSubmit(e: ReactEventFromInput) =
      e.preventDefaultCB >>
        $.modState(s => newGame(s.text.toInt))
    def max(n1: Float, n2: Float) = if (n1>n2) n1 else n2
    def renderWinMessage(state: State) = {
      val board = state.board
      val cn =
        if (board.forall(_ == false))
          "win"
        else
          "play"

      val eff_nr = state.genClicks.toFloat/max(state.clicks.toFloat, 1.0f)*100
      val eff = f"$eff_nr%1.1f"
      var eff_text = ""
      if (eff_nr>=100)
        eff_text = "Nice!"
      else
        eff_text = "Sorry!"
      div(
        className := cn,
        h2("You win!"),
        div(s"You clicked ${state.clicks} times."),
        div(s"Generator clicked ${state.genClicks} times."),
        div(s"Your efficiency is ${eff}%. ${eff_text}"),
        renderNewGame(state)
      )
    }

    def renderNewGame(state: State) =
      div(
        className := "newGameContainer",
        div(
          "Enter the amount of random clicks on the board to start a new game."
        ),
        form(onSubmit ==> handleSubmit,
          input(
            onChange ==> onChangeHandler,
            value := state.text),
          button("Start")
        )
      )

    def render(state: State) =
      div(
        renderNewGame(state),
        renderWinMessage(state),
        renderBoard(state.board)
      )
  }

  private val component = ScalaComponent.builder[Unit]("LightsOut")
    .initialState(newGame(4))
    .renderBackend[Backend]
    .build

  def renderInto(c: Element): Unit = {
    component().renderIntoDOM(c)
  }

}