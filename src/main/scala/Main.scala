import intro._
import org.scalajs.dom._

object Main {

  def main(args: Array[String]): Unit = {
    LightsOut.renderInto(document.getElementById("lout"))
  }

}
